package com.qianxun;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.qianxun.mapper")
@SpringBootApplication
public class QianxunApplication {

    public static void main(String[] args) {
        SpringApplication.run(QianxunApplication.class, args);
    }

}
