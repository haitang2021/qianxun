package com.qianxun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@TableName("t_traveller")
@ApiModel(value = "Traveller对象", description = "")
public class Traveller implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("真实姓名")
    private String mRealName;

    @ApiModelProperty("证件号")
    private String mNmu;

    @ApiModelProperty("手机号")
    private String mPhone;

    @ApiModelProperty("性别")
    private String mGender;

    @ApiModelProperty("出生日期")
    private LocalDate mBirthday;

    @ApiModelProperty("当前状态")
    private String mState;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getmRealName() {
        return mRealName;
    }

    public void setmRealName(String mRealName) {
        this.mRealName = mRealName;
    }
    public String getmNmu() {
        return mNmu;
    }

    public void setmNmu(String mNmu) {
        this.mNmu = mNmu;
    }
    public String getmPhone() {
        return mPhone;
    }

    public void setmPhone(String mPhone) {
        this.mPhone = mPhone;
    }
    public String getmGender() {
        return mGender;
    }

    public void setmGender(String mGender) {
        this.mGender = mGender;
    }
    public LocalDate getmBirthday() {
        return mBirthday;
    }

    public void setmBirthday(LocalDate mBirthday) {
        this.mBirthday = mBirthday;
    }
    public String getmState() {
        return mState;
    }

    public void setmState(String mState) {
        this.mState = mState;
    }

    @Override
    public String toString() {
        return "Traveller{" +
            "id=" + id +
            ", mRealName=" + mRealName +
            ", mNmu=" + mNmu +
            ", mPhone=" + mPhone +
            ", mGender=" + mGender +
            ", mBirthday=" + mBirthday +
            ", mState=" + mState +
        "}";
    }
}
