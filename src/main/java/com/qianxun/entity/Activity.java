package com.qianxun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@TableName("t_activity")
@ApiModel(value = "Activity对象", description = "")
public class Activity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("标签id")
    private Integer tagId;

    @ApiModelProperty("活动轮播图片")
    private String mImageUrl;

    @ApiModelProperty("活动名称")
    private String mName;

    @ApiModelProperty("活动简介")
    private String mDesc;

    @ApiModelProperty("活动人数上限")
    private Integer mNumLimit;

    @ApiModelProperty("年龄不大于多少岁")
    private Integer mAgeGreater;

    @ApiModelProperty("年龄不小于多少岁")
    private Integer mAgeLess;

    @ApiModelProperty("集合地点")
    private String mAssemblePlace;

    @ApiModelProperty("解散地点")
    private String mDismissPlace;

    @ApiModelProperty("行程天数")
    private String mDays;

    @ApiModelProperty("活动排期")
    private String mPlan;

    @ApiModelProperty("活动报名咨询群二维码")
    private String mGroupQrCode;

    @ApiModelProperty("活动报名咨询客服二维码")
    private String mServiceQrCode;

    @ApiModelProperty("价格")
    private Double mPrice;

    @ApiModelProperty("价格2")
    private Double mPrice2;

    @ApiModelProperty("亮点特色")
    private String mFeature;

    @ApiModelProperty("行程安排")
    private String mSchedule;

    @ApiModelProperty("费用说明")
    private String mExpense;

    @ApiModelProperty("行前必看")
    private String mNotice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }
    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }
    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }
    public String getmDesc() {
        return mDesc;
    }

    public void setmDesc(String mDesc) {
        this.mDesc = mDesc;
    }
    public Integer getmNumLimit() {
        return mNumLimit;
    }

    public void setmNumLimit(Integer mNumLimit) {
        this.mNumLimit = mNumLimit;
    }
    public Integer getmAgeGreater() {
        return mAgeGreater;
    }

    public void setmAgeGreater(Integer mAgeGreater) {
        this.mAgeGreater = mAgeGreater;
    }
    public Integer getmAgeLess() {
        return mAgeLess;
    }

    public void setmAgeLess(Integer mAgeLess) {
        this.mAgeLess = mAgeLess;
    }
    public String getmAssemblePlace() {
        return mAssemblePlace;
    }

    public void setmAssemblePlace(String mAssemblePlace) {
        this.mAssemblePlace = mAssemblePlace;
    }
    public String getmDismissPlace() {
        return mDismissPlace;
    }

    public void setmDismissPlace(String mDismissPlace) {
        this.mDismissPlace = mDismissPlace;
    }
    public String getmDays() {
        return mDays;
    }

    public void setmDays(String mDays) {
        this.mDays = mDays;
    }
    public String getmPlan() {
        return mPlan;
    }

    public void setmPlan(String mPlan) {
        this.mPlan = mPlan;
    }
    public String getmGroupQrCode() {
        return mGroupQrCode;
    }

    public void setmGroupQrCode(String mGroupQrCode) {
        this.mGroupQrCode = mGroupQrCode;
    }
    public String getmServiceQrCode() {
        return mServiceQrCode;
    }

    public void setmServiceQrCode(String mServiceQrCode) {
        this.mServiceQrCode = mServiceQrCode;
    }
    public Double getmPrice() {
        return mPrice;
    }

    public void setmPrice(Double mPrice) {
        this.mPrice = mPrice;
    }
    public Double getmPrice2() {
        return mPrice2;
    }

    public void setmPrice2(Double mPrice2) {
        this.mPrice2 = mPrice2;
    }
    public String getmFeature() {
        return mFeature;
    }

    public void setmFeature(String mFeature) {
        this.mFeature = mFeature;
    }
    public String getmSchedule() {
        return mSchedule;
    }

    public void setmSchedule(String mSchedule) {
        this.mSchedule = mSchedule;
    }
    public String getmExpense() {
        return mExpense;
    }

    public void setmExpense(String mExpense) {
        this.mExpense = mExpense;
    }
    public String getmNotice() {
        return mNotice;
    }

    public void setmNotice(String mNotice) {
        this.mNotice = mNotice;
    }

    @Override
    public String toString() {
        return "Activity{" +
            "id=" + id +
            ", tagId=" + tagId +
            ", mImageUrl=" + mImageUrl +
            ", mName=" + mName +
            ", mDesc=" + mDesc +
            ", mNumLimit=" + mNumLimit +
            ", mAgeGreater=" + mAgeGreater +
            ", mAgeLess=" + mAgeLess +
            ", mAssemblePlace=" + mAssemblePlace +
            ", mDismissPlace=" + mDismissPlace +
            ", mDays=" + mDays +
            ", mPlan=" + mPlan +
            ", mGroupQrCode=" + mGroupQrCode +
            ", mServiceQrCode=" + mServiceQrCode +
            ", mPrice=" + mPrice +
            ", mPrice2=" + mPrice2 +
            ", mFeature=" + mFeature +
            ", mSchedule=" + mSchedule +
            ", mExpense=" + mExpense +
            ", mNotice=" + mNotice +
        "}";
    }
}
