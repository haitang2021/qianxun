package com.qianxun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@TableName("t_admin")
@ApiModel(value = "Admin对象", description = "")
public class Admin implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("密码")
    private String mPassword;

    @ApiModelProperty("用户名")
    private String mUserName;

    @ApiModelProperty("管理员角色")
    private Integer mRole;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }
    public String getmUserName() {
        return mUserName;
    }

    public void setmUserName(String mUserName) {
        this.mUserName = mUserName;
    }
    public Integer getmRole() {
        return mRole;
    }

    public void setmRole(Integer mRole) {
        this.mRole = mRole;
    }

    @Override
    public String toString() {
        return "Admin{" +
            "id=" + id +
            ", mPassword=" + mPassword +
            ", mUserName=" + mUserName +
            ", mRole=" + mRole +
        "}";
    }
}
