package com.qianxun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@TableName("t_user")
@ApiModel(value = "User对象", description = "")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("头像图片url")
    private String mImageUrl;

    @ApiModelProperty("用户名")
    private String mUserName;

    @ApiModelProperty("登录手机号")
    private String mPhone;

    @ApiModelProperty("注册时间")
    private String mRegisterTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }
    public String getmUserName() {
        return mUserName;
    }

    public void setmUserName(String mUserName) {
        this.mUserName = mUserName;
    }
    public String getmPhone() {
        return mPhone;
    }

    public void setmPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public String getmRegisterTime() {
        return mRegisterTime;
    }

    public void setmRegisterTime(String mRegisterTime) {
        this.mRegisterTime = mRegisterTime;
    }

    @Override
    public String toString() {
        return "User{" +
            "id=" + id +
            ", mImageUrl=" + mImageUrl +
            ", mUserName=" + mUserName +
            ", mPhone=" + mPhone +
                ", mRegisterTime=" + mRegisterTime +
        "}";
    }
}
