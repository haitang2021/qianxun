package com.qianxun.service;

import com.qianxun.entity.Traveller;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
public interface ITravellerService extends IService<Traveller> {

}
