package com.qianxun.service.impl;

import com.qianxun.entity.Traveller;
import com.qianxun.mapper.TravellerMapper;
import com.qianxun.service.ITravellerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@Service
public class TravellerServiceImpl extends ServiceImpl<TravellerMapper, Traveller> implements ITravellerService {

}
