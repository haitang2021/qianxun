package com.qianxun.service.impl;

import com.qianxun.entity.Ad;
import com.qianxun.mapper.AdMapper;
import com.qianxun.service.IAdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@Service
public class AdServiceImpl extends ServiceImpl<AdMapper, Ad> implements IAdService {

}
