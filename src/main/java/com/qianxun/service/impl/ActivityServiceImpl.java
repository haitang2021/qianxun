package com.qianxun.service.impl;

import com.qianxun.entity.Activity;
import com.qianxun.mapper.ActivityMapper;
import com.qianxun.service.IActivityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@Service
public class ActivityServiceImpl extends ServiceImpl<ActivityMapper, Activity> implements IActivityService {

}
