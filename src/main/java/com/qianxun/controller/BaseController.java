package com.qianxun.controller;

import com.qianxun.entity.Admin;
import com.qianxun.service.impl.AdServiceImpl;
import com.qianxun.service.impl.AdminServiceImpl;
import com.qianxun.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
public class BaseController {
    @Autowired
    AdminServiceImpl adminService;
    @Autowired
    UserServiceImpl userService;
    @Autowired
    AdServiceImpl adService;
    @Autowired
    HttpSession session;

    public Map<Object,Object> getErrorResponse(int code,String msg){
        Map<Object,Object> map=new HashMap<>();
        map.put("msg",msg);
        map.put("code",code);
        return map;
    }

    public Map<Object,Object> getRightResponse(String msg){
        return getRightResponse("",msg);
    }

    public Map<Object,Object> getRightResponse(Object data,String msg){
        return getRightResponse(1,data,msg);
    }

    public Map<Object,Object> getRightResponse(int count,Object data,String msg){
        Map<Object,Object> map=new HashMap<>();
        map.put("code",0);
        map.put("msg",msg);
        map.put("data",data);
        map.put("count",count);
        return map;
    }

    public void setAdmin(Admin admin){
        session.setAttribute("Admin",admin);
//        session.setMaxInactiveInterval(7776000);//单位秒
        session.setMaxInactiveInterval(0);//设置0表示永远不过期
    }

    public Admin getAdmin(){
        return (Admin) session.getAttribute("Admin");
    }
}
