package com.qianxun.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@Controller
@RequestMapping("/qianxun/setting")
public class SettingController {

}
