package com.qianxun.controller;


import com.qianxun.entity.Ad;
import com.qianxun.entity.User;
import com.qianxun.utils.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@Controller
@RequestMapping("/qianxun/ad")
public class AdController extends  BaseController{
    @RequestMapping("/")
    public String ad(){
        return "admin/ad";
    }

    @RequestMapping("/doGetAd")
    @ResponseBody
    public Map<Object,Object> doGetAd(){
        List<Ad> adList =  adService.lambdaQuery().list();;
        return getRightResponse(adList.size(),adList,"获取广告图信息成功");
    }

    @RequestMapping("/adAdd")
    public String adAdd(){
        return "admin/ad_add";
    }

    @RequestMapping("/doAddAd")
    @ResponseBody
    public Map<Object,Object> doAddAd(String mImageUrl,String  mLinkUrl ,Integer mOrder){
        mLinkUrl=   mLinkUrl==null?"":mLinkUrl.trim();
        mOrder=   mOrder==null?0:mOrder;
        Ad ad=new Ad();
        ad.setmImageUrl(mImageUrl);
        ad.setmLinkUrl(mLinkUrl);
        ad.setmOrder(mOrder);
        if( adService.save(ad) ){
            return getRightResponse("添加成功");
        }else{
            return getErrorResponse(1000,"添加失败");
        }
    }

    @RequestMapping("/doEditAd")
    @ResponseBody
    public Map<Object,Object> doEditAd(Integer id,String  mLinkUrl ,String mOrder){
        mLinkUrl=   mLinkUrl==null?"":mLinkUrl.trim();
        mOrder=   mOrder==null?"":mOrder.trim();
        if(!StringUtils.isNumber(mOrder)){
            return getErrorResponse(1001,"修改失败，排序请输入数字");
        }
        Ad ad=adService.getById(id);
        ad.setmLinkUrl(mLinkUrl);
        ad.setmOrder(Integer.parseInt(mOrder));
        if( adService.updateById(ad)){
            return getRightResponse("修改成功");
        }else{
            return getErrorResponse(1000,"修改失败");
        }
    }

    @RequestMapping("/doDeleteAd")
    @ResponseBody
    public Map<Object,Object> doDeleteAd(Integer id){
        if( adService.removeById(id) ){
            return getRightResponse("删除成功");
        }else{
            return getErrorResponse(1000,"删除失败");
        }
    }
}
