package com.qianxun.controller;


import com.qianxun.entity.Admin;
import com.wf.captcha.utils.CaptchaUtil;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@Controller
@RequestMapping("/qianxun/admin")
public class AdminController extends BaseController {
    @RequestMapping("/")
    public String index(){
        return "admin/index";
    }

    @ResponseBody
    @RequestMapping("/doGetMenu")
    public String doGetMenu(){
       return  "{\n" +
                "  \"homeInfo\": {\n" +
                "    \"title\": \"首页\",\n" +
                "    \"href\": \"welcome\"\n" +
                "  },\n" +
                "  \"logoInfo\": {\n" +
                "    \"title\": \"千寻\",\n" +
                "    \"image\": \"/layuimini-v2/images/logo.png\",\n" +
                "    \"href\": \"\"\n" +
                "  },\n" +
                "  \"menuInfo\": [\n" +
                "    {\n" +
                "      \"title\": \"\",\n" +
                "      \"icon\": \"fa fa-address-book\",\n" +
                "      \"href\": \"\",\n" +
                "      \"target\": \"_self\",\n" +
                "      \"child\": [\n" +
                "        {\n" +
                "          \"title\": \"首页\",\n" +
                "          \"href\": \"welcome\",\n" +
                "          \"icon\": \"fa fa-home\",\n" +
                "          \"target\": \"_self\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"title\": \"用户管理\",\n" +
                "          \"href\": \"/qianxun/user/\",\n" +
                "          \"icon\": \"fa fa-file-text\",\n" +
                "          \"target\": \"_self\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"title\": \"广告图管理\",\n" +
                "          \"href\": \"/qianxun/ad/\",\n" +
                "          \"icon\": \"fa fa-file-text\",\n" +
                "          \"target\": \"_self\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"title\": \"表格示例\",\n" +
                "          \"href\": \"page/table.html\",\n" +
                "          \"icon\": \"fa fa-file-text\",\n" +
                "          \"target\": \"_self\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"title\": \"表单示例\",\n" +
                "          \"href\": \"\",\n" +
                "          \"icon\": \"fa fa-calendar\",\n" +
                "          \"target\": \"_self\",\n" +
                "          \"child\": [\n" +
                "            {\n" +
                "              \"title\": \"普通表单\",\n" +
                "              \"href\": \"page/form.html\",\n" +
                "              \"icon\": \"fa fa-list-alt\",\n" +
                "              \"target\": \"_self\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"title\": \"分步表单\",\n" +
                "              \"href\": \"page/form-step.html\",\n" +
                "              \"icon\": \"fa fa-navicon\",\n" +
                "              \"target\": \"_self\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"title\": \"登录模板\",\n" +
                "          \"href\": \"\",\n" +
                "          \"icon\": \"fa fa-flag-o\",\n" +
                "          \"target\": \"_self\",\n" +
                "          \"child\": [\n" +
                "            {\n" +
                "              \"title\": \"登录-1\",\n" +
                "              \"href\": \"page/login-1.html\",\n" +
                "              \"icon\": \"fa fa-stumbleupon-circle\",\n" +
                "              \"target\": \"_blank\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"title\": \"登录-2\",\n" +
                "              \"href\": \"page/login-2.html\",\n" +
                "              \"icon\": \"fa fa-viacoin\",\n" +
                "              \"target\": \"_blank\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"title\": \"登录-3\",\n" +
                "              \"href\": \"page/login-3.html\",\n" +
                "              \"icon\": \"fa fa-tags\",\n" +
                "              \"target\": \"_blank\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"title\": \"异常页面\",\n" +
                "          \"href\": \"\",\n" +
                "          \"icon\": \"fa fa-home\",\n" +
                "          \"target\": \"_self\",\n" +
                "          \"child\": [\n" +
                "            {\n" +
                "              \"title\": \"404页面\",\n" +
                "              \"href\": \"page/404.html\",\n" +
                "              \"icon\": \"fa fa-hourglass-end\",\n" +
                "              \"target\": \"_self\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"title\": \"其它界面\",\n" +
                "          \"href\": \"\",\n" +
                "          \"icon\": \"fa fa-snowflake-o\",\n" +
                "          \"target\": \"\",\n" +
                "          \"child\": [\n" +
                "            {\n" +
                "              \"title\": \"按钮示例\",\n" +
                "              \"href\": \"page/button.html\",\n" +
                "              \"icon\": \"fa fa-snowflake-o\",\n" +
                "              \"target\": \"_self\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"title\": \"弹出层\",\n" +
                "              \"href\": \"page/layer.html\",\n" +
                "              \"icon\": \"fa fa-shield\",\n" +
                "              \"target\": \"_self\"\n" +
                "            }\n" +
                "          ]\n" +
                "        }\n" +
                "      ]\n" +
                "    }\n" +
                "  ]\n" +
                "}";
    }

    @RequestMapping("/welcome")
    public String welcome(){
        return "admin/welcome";
    }




    @RequestMapping("/login")
    public String login(){
        return "admin/login";
    }


    @RequestMapping("/captcha")
    public void captcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CaptchaUtil.out(request, response);
    }

    @ResponseBody
    @RequestMapping("/doLogin")
    public Map<Object,Object> doLogin(String username, String password, String captcha, HttpServletRequest request, HttpServletResponse response){
        if (!CaptchaUtil.ver(captcha, request)) {
             return getErrorResponse(1000,"验证码不正确");
         }
        Admin admin=adminService.lambdaQuery().eq(Admin::getmUserName,username).eq(Admin::getmPassword,password).one();
        if(admin==null){
            return getErrorResponse(1001,"用户名或密码错误");
        }
        admin.setmPassword("");
        setAdmin(admin);
        return getRightResponse("登录成功");
    }


    @RequestMapping("/modifyPassword")
    public String modifyPassword(){
        return "admin/user-password";
    }

    @ResponseBody
    @RequestMapping("/doModifyPassword")
    public Map<Object,Object> doModifyPassword(String old_password, String new_password, String again_password, HttpServletRequest request, HttpServletResponse response){
        if(!new_password.equals(again_password))
            return getErrorResponse(1000,"两次输入的新密码不一样");
        Admin admin=adminService.lambdaQuery().eq(Admin::getmUserName,getAdmin().getmUserName()).eq(Admin::getmPassword,old_password).one();
        if(admin==null){
            return getErrorResponse(1001,"原密码错误");
        }
        admin.setmPassword(new_password);
        adminService.updateById(admin);
        return getRightResponse("密码修改成功");
    }

    @RequestMapping("/doLogout")
    public String doLogout(){
        setAdmin(null);
        return login();
    }

}
