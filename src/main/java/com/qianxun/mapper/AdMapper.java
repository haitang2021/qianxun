package com.qianxun.mapper;

import com.qianxun.entity.Ad;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
public interface AdMapper extends BaseMapper<Ad> {

}
